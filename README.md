# ION build container

Use this docker container to test ION compilation.

It's possible to test the compilation with the following compiler:
- gcc-6
- gcc-7
- gcc-8

## Usage

### Create the container image:

The following build-arg variables are available:
- `gcc_version`: accepted `[ 6 | 7 | 8 ]`, default: `6`
- `parallelism`: make -j parameter. default: `1`
- `ion_version`: default: `3.6.2`

The image tag name (`-t` option) is arbitrary. Just use a meaningful value.

Example:

`docker build -t ion-build-gcc-8 --build-arg ion_version=3.6.2 --build-arg parallelism=8 --build-arg gcc_version=8 .`

Using default values:

`docker build -t ion-build-default .`

### Run the container

The container default run command copies the ION build output to `/home/build/ion_package`.

Use the `-v` option to mount a local folder into the container so that you get the build output in your local filesystem.
Use the image tag name previously defined with the `docker build` command.

Example:

`docker run -ti --rm -v /path/to/local/folder:/home/build/ion_package ion-build-default`

### Run further tests

You can mount other local folders into the container and run commands.

Example:

The following command mounts the al_bp and dtnperf folders into the container and
runs a interactive shell (`/bin/bash`) where you can test al_bp and dtnperf compilation.

`docker run -ti --rm -v /path/to/al_bp:/home/build/al_bp -v /path/to/dtnperf:/home/build/dtnperf ion-build-default /bin/bash`

