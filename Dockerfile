# AS OF MAY 2018
# gcc:6 is Debian 8 (Jessie) with gcc 6.5.0 glibc 2.19 cmake 3.0.2
# gcc:7 is Debian 9 (Stretch) with gcc 7.4.0 glibc 2.24 cmake 3.7.2
# gcc:8 is Debian 9 (Stretch) with gcc 8.3.0 glibc 2.24 cmake 3.7.2

# default version is 6
ARG gcc_version=6

FROM gcc:${gcc_version}

RUN apt-get update && apt-get -y upgrade

VOLUME /home/build/ion_package

# ION
# ARG default values:
ARG ion_version=3.7.0
ARG parallelism=1

# Download sources
WORKDIR /home/build/
RUN wget "http://downloads.sourceforge.net/project/ion-dtn/ion-$ion_version.tar.gz"
RUN mkdir -p ion && tar xvaf ion-$ion_version.tar.gz -C ion --strip-components=1

# Compile ION
WORKDIR /home/build/ion/
# check compiler version
RUN gcc --version -v
# add bpP.h to the list of installed headers as it is used by al_bp
RUN sed -i'' -e 's:bp/library/bpP.h::' -e 's-\(bp/include/bp.h\)-\1 bp/library/bpP.h-' Makefile.in
RUN ./configure && make -j${parallelism}
# install to system folder for dtnperf compilation on this container
RUN make install && ldconfig

# install to a separate folder to prepare package
# if VOLUME is mounted, the package will be available in the host fs
CMD DESTDIR=/home/build/ion_package make install

